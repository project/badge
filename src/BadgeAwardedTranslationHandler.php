<?php

namespace Drupal\badge;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for badge_awarded.
 */
class BadgeAwardedTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
