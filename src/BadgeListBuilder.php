<?php

namespace Drupal\badge;

use Drupal\Core\Link;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Badge entities.
 *
 * @ingroup badge
 */
class BadgeListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Badge ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\badge\Entity\Badge */
    $row['id'] = $entity->id();

    $row['name'] = Link::fromTextAndUrl($entity->label(), new Url(
      'entity.badge.edit_form', [
        'badge' => $entity->id(),
      ]
    ));
    return $row + parent::buildRow($entity);
  }

}
