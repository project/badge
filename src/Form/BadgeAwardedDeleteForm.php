<?php

namespace Drupal\badge\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Badge awarded entities.
 *
 * @ingroup badge
 */
class BadgeAwardedDeleteForm extends ContentEntityDeleteForm {


}
