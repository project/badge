<?php

namespace Drupal\badge\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Badge edit forms.
 *
 * @ingroup badge
 */
class BadgeForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Badge.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Badge.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.badge.canonical', ['badge' => $entity->id()]);
  }

}
