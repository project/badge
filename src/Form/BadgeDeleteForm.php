<?php

namespace Drupal\badge\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Badge Name entities.
 *
 * @ingroup badge
 */
class BadgeDeleteForm extends ContentEntityDeleteForm {


}
