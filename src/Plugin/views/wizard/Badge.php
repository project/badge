<?php

namespace Drupal\badge\Plugin\views\wizard;

use Drupal\views\Plugin\views\wizard\WizardPluginBase;

/**
 * Tests creating badge views with the wizard.
 *
 * @ViewsWizard(
 *   id = "badge",
 *   base_table = "badge_field_data",
 *   title = @Translation("Badge")
 * )
 */
class Badge extends WizardPluginBase {

}
