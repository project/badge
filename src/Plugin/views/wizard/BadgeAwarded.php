<?php

namespace Drupal\badge\Plugin\views\wizard;

use Drupal\views\Plugin\views\wizard\WizardPluginBase;

/**
 * Tests creating badge views with the wizard.
 *
 * @ViewsWizard(
 *   id = "badge_awarded",
 *   base_table = "badge_awarded_field_data",
 *   title = @Translation("Badge Awarded")
 * )
 */
class BadgeAwarded extends WizardPluginBase {

}
